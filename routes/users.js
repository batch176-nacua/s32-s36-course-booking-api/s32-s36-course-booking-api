// Dependencies and Modules
	const express = require('express') 
	const controller = require('../controllers/users') 
	const auth = require('../auth.js')

// Routing Component
	//Implement a new routing system dedicated for the users collection
	const route = express.Router()

// POST/create routes a user
	route.post('/register', (req, res) => {
		console.log(req.body)
		let userData = req.body
		//Invoke the controller function you wish to execute
		controller.register(userData).then(result => {
			res.send(result)
		})
	})

// Routes for User Athentication(LOGIN)
	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(result => res.send(result) )
	})

// GET/retrieve routes
	//GET the user's details
	route.get('/details', auth.verify, (req, res) => {
		//req.user is inside auth verify()
		controller.getProfile(req.user.id).then(result => res.send(result) )
	})

// Enroll our Registered Users
	//(req, res) is in controller
	route.post('/enroll', auth.verify, controller.enroll)

// Get logged user's enrollments
	route.get('/getEnrollments', auth.verify, controller.getEnrollments)

// PUT/update routes


// DEL/destroy routes


// Expose Route System
	module.exports = route