// Dependencies and Modules
	const express = require('express')
	const CourseController = require('../controllers/courses')
	const auth = require('../auth')

	///destructure the actual function that we need to use
	const { verify, verifyAdmin} = auth

// Routing Component
	const route = express.Router()

// Route for Creating a Course
	route.post('/create', verify, verifyAdmin, (req, res) => {
		CourseController.addCourse(req.body).then(result => res.send(result))
	})

// Route for RETRIEVING courses
	//ALL COURSES
	//No need to verify users (admin or not)(logged in/not logged in) because All courses are available to all
	route.get('/all', (req, res) => {
		CourseController.getAllCourses().then(result => res.send(result))
	})

	//ALL ACTIVE COURSES
	route.get('/active', (req, res) => {
		CourseController.getActiveCourses().then(result => res.send(result))
	})

	//SPECIFIC course via ID
	//req.params
	// /:628b2d38417d7677788b6f8c => id of the course. Route in client or you can just put something in path variables in PARAMS area
	route.get('/:courseId', (req, res) => {
		console.log(req.params.courseId)
		//We can retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the URL
		CourseController.getCourse(req.params.courseId).then(result => res.send(result))
	})

// Updating Courses
	route.put('/:courseId', verify, verifyAdmin, (req, res) => {
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
	})

//Archiving a course
	route.put('/:courseId/archive', verify, verifyAdmin, (req, res) => {
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result))
	})

//Activate a course
	route.put('/:courseId/activate', verify, verifyAdmin, (req, res) => {
		CourseController.activateCourse(req.params.courseId).then(result => res.send(result))
	})



// Expose Route System
	module.exports = route