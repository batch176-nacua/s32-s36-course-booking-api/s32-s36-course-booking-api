//(1st step) Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors')

	//After you setup your routing system
	const userRoutes = require('./routes/users')
	const courseRoutes = require('./routes/courses')

//(3rd) Environment Setup
	dotenv.config();
	let account = process.env.mongoDBInfo
	//PORT in .env must always be capitalized
	const port = process.env.PORT

//(2nd) Server Setup
	const app = express();
	//Middleware
	//the app should be able to recognize data written in a json format
	app.use(express.json())

	//It enables all origins/address/URL of the client request
	//NOTE: Must be at the top of the routes so that this will be read first
	app.use(cors())

	//Example cors option instead of enables all
	//For more context: https://www.npmjs.com/package/cors or https://expressjs.com/en/resources/middleware/cors.html
	// let corsOptions = {
	// 	origin: ['http://localhost:4000', 'http://example.com']
	// }

//(5th) Database Connection
	mongoose.connect(account)
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log(`MongoDB Successfully Connected`));

//(6th) Backend Routes
	//After you initialize your routing system in inside dependencies/modules
	app.use('/users', userRoutes)
	app.use('/courses', courseRoutes)
//(4th) Server Gateway Response
	app.listen(port, () => {
		console.log(`API is Hosted on Port: ${port}`);
	})

	app.get('/', (req, res) => {
		res.send(`Welcome to Enrollment-System`)
	})