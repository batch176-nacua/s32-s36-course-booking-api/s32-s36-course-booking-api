//JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our user to access or disallow access to certain parts of our app

// Dependencies
const jwt = require('jsonwebtoken')

//User Defined string data that will be used to create JWT
//Used in the algorith for encrypting our data which it difficult to decode the informatiton without the defined secret keyword
const secret = 'CourseBookingAPI'

//Token Creation
 //Analogy: Pack the gift and provide a lock with the secret code as the key

 module.exports.createAccessToken = (user) => {
 	//Check if we can receive the details of the user from our login
 	console.log(user)

 	//Payload: object to contain some details of our user
 	//If you can call the _id, you can already access all the fields within the _id so there's no need to put all the field
 	const data = {
 		id: user._id,
 		email: user.email,
 		isAdmin: user.isAdmin
 	}

 	//Signature: Generate a jwt's sign method
 	//NOTE: If no expiration in login/ autologout if idle then just leave it {} blank, if there is then use https://www.npmjs.com/package/jsonwebtoken so that you can study it first before putting something xd
 	return jwt.sign(data, secret, {})
 }

 //Token Verification
 	//Analogy: To receive the gift and if he can open the lock using the secret key, if not then deny request

 module.exports.verify = (req, res, next) => {
 	// The token is retrieved from the request header
 	// authorozitaion is still under in headers in POSTMAN, it's independent in the postman because it is mostly used
 	console.log(req.headers.authorization)

 	let token = req.headers.authorization

 	//This if statement will first check if the token variable undefined or a proper jwt
 	if(typeof token === "undefined"){
 		return res.send({ auth: "Failed. No Token"})
 	}else {
 		console.log(token)
 		//Bearer askldjklwajeklqeasd or <token value>
 		//to get the ACTUAL TOKEN => Use slice to cut of the "bearer " using slice(<startingPos>, <endingPos>)
 		token = token.slice(7, token.length)

 		//Validate the token using the "verify" method decrypting the token using the secret code
 		jwt.verify(token, secret, function(err, decodedToken) {
 			if(err) {
 				return res.send({
 					auth: "Failed",
 					message: err.message
 				})
 			} else {
 				console.log(decodedToken)

 				//User property will be added to request object and will contain our decoded token, it can be accessed in the verify
 				req.user = decodedToken
 				//Middleware function next() will let us proceed to the next middleware or controller
 				next()
 			}
 		})

 	}
 }


 //Verify if the user is admin and will be used also as middleware

 module.exports.verifyAdmin = (req, res, next) => {
 	//NOTE: You can only have req.user for any middleware or controller that comes after verify
 	if(req.user.isAdmin){
 		//If the logged user, based on his token is an admin, we will proceed to the next middleware/controller
 		next()
 	}else return res.send({ auth: "Failed", message: "Action Forbidden"})
 }

