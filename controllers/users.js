// Dependencies and Modules
	const User = require('../models/User') //Model for User
	const Course = require('../models/Course')
	const bcrypt = require('bcrypt') 
	const dotenv = require('dotenv')
	const auth = require('../auth.js') 

//Environment Variables Setup
	dotenv.config()
	let salt = parseInt(process.env.SALT)

// Functionalities [CREATE]
	//1. Register New Acc
	module.exports.register = (userData) => {
		let fName = userData.firstName		
		let lName = userData.lastName
		let email = userData.email
		let passW = userData.password
		let mobil = userData.mobileNo
		//Create a new instance of an object appended with the model, for us to create a new document that will inherit properties stated inside the user schema.
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			mobileNo: mobil 
		});

		return newUser.save().then((success, err) => {
			if(success) {
				return success
			}else return {message: 'Failed to Register Account'}
		})
	}

// User Authentication
	/* Steps:
	1. Check the db if the user email exists
	2. Compare the password provided in the login form with password stored in DB
	3. Generate/return a JSON web token if the user is successfully logged in, return false if not
	*/

	//data is the req.body in route but there's no need to be the same name as long as it's in the same order within the parameter, so don't be confuse
	module.exports.loginUser = (data) => {
		//findOne method returns the first record in the collection
		return User.findOne({ email: data.email}).then(result => {
			if(result == null){
				//No email found
				return false
			}else {
				//compareSync() method is from bcrypt to comparing the non encrypted password from the user input and in the database. 
				//NOTE: variable starting with is (isVariableName) will be in boolean result so true/false
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

				//If password match, return token
				if(isPasswordCorrect){
					return { accessToken: auth.createAccessToken(result.toObject() )}
				}else return false //Wrong Password
			}
		})
	}
// Functionalities 
	//GET user's profile
	/* Steps:
	1. Find the document in the db using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result backk to the client/frontend
	*/
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			//Change the value of the user's password to an empty string
			result.password = ''
			return result
		})
	}

// Enroll Registered User
	/*Steps
	1. Look for the user via ID.
		-push the details to a new enrollment subdocument in the user
	2. Look for the course via ID. 
		-push the details of the user who's trying to enroll, push to a new enrollees subdocument in the course
	3. When both saving documents are successful, we send a message to a client. True if successful, false if not
	*/

	module.exports.enroll = async (req, res) => {
		//console.log('Test enroll route')
		console.log(req.user) //user's id from the decoded token after verify()
		console.log(req.body.courseId) //course ID from our request body

		if(req.user.isAdmin){
			return res.send({ message: "Action Forbidden: Must be a Normal User"})
		}

		let isUserUpdated = await User.findById(req.user.id).then( user => {
			console.log(req.user.id)
			//Add the courseId in an object and push that object into user's enrollments array
			let newEnrollment = {
				courseId: req.body.courseId
			}

			user.enrollments.push(newEnrollment) //enrollments is from the user's model
			console.log(user.enrollments)
			//If successfull then isUserUpdated returns to true
			return user.save().then(user => true).catch(err => err.message)

			//If isUserUpdated doest not containt the boolean true, it will stop our process and return a message to our client
			if(isUserUpdated !== true){
				return res.send({ message: isUserUpdated} )
			}


		})

		let isCourseUpdated = await Course.findById(req.body.courseId).then ( course => {
			let newEnrollee = {
				userId: req.user.id
			}

			course.enrollees.push(newEnrollee)

			return course.save().then(course => true).catch(err => err.message)

			if(isCourseUpdated !== true){
				return res.send({ message: isCourseUpdated})
			}
		})

		//Send message to client if both isUserUpdated & isCourseUpdated is successfully true
		if(isUserUpdated && isCourseUpdated){
			return res.send({ message: 'Enrolled Sucessfully!'})
		}
	}

//Logged in User's Enrollments
	module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(err => res.send(err))
	}


// Functionalities [UPDATE]


// Functionalities [DELETE]