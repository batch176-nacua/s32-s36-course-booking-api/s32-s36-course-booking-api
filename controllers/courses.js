// Dependencies and Modules
	const Course = require('../models/Course')

// Functionalities [CREATE]
	/*Create a new Course
	1. Create new Course object using the mongoose model and the information from the reuest body
	2. Save the new Course to the database 
	*/
	module.exports.addCourse = (data) => {
		let newCourse = new Course({
			name: data.name,
			description: data.description,
			price: data.price
		})

		return newCourse.save().then((success, err) => {
			if(err){
				return false
			}else return true
		}).catch(error => error.message) //error => error
	}


// Functionalities [RETRIEVE]
	//Retrieve ALL the courses
	module.exports.getAllCourses = () => {
		//Empty Criteria {} 
		return Course.find({}).then(result => {
			return result
		})
	}

	//Retrieve ALL ACTIVE courses
	module.exports.getActiveCourses = () => {
		return Course.find({isActive: true}).then(result => {
			return result
		})
	}

	//Retrieve SPECIFIC course
	module.exports.getCourse = (data) => {
		return Course.findById(data).then(result =>{
			return result
		})
	}


// Functionalities [UPDATE]
	/*Steps
	1. Create a variable which will contain the info retrieved from the req.body
	2. Find and Update the course using the courseId retrieved from req.params and the variable containing info from req.body 
	*/
	//courseId = req.params.courseId and data = req.body from routes
	module.exports.updateCourse = (courseId, data) => {


		let updatedCourse = {
			name: data.name,
			description: data.description,
			price: data.price
		}
		//Syntax: findByIdAndUpdate(docId, updatesToBeApplied)
		return Course.findByIdAndUpdate(courseId, updatedCourse).then((success, err) => {
			if(err) {
				return false
			} else return true
		}).catch(error => error.message)
	}

	//Archiving a Course, isActive = true to false
	module.exports.archiveCourse = (courseId) => {
		let updateStatus = {
			isActive: false
		}
		//If you want to delete it instead of archiving, just use findByIdandRemove()
		return Course.findByIdAndUpdate(courseId, updateStatus).then((success, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	}

	//Activating a Course, isActive = false to true
	module.exports.activateCourse = (courseId) => {
		let updateStatus = {
			isActive: true
		}
		//If you want to delete it instead of archiving, just use findByIdandRemove()
		return Course.findByIdAndUpdate(courseId, updateStatus).then((success, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	}

// Functionalities [DELETE]